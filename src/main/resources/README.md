Links
# GitLab repository: https://gitlab.com/group-of-users/cucumberserenity.git
# Serenity report: https://group-of-users.gitlab.io/-/cucumberserenity/-/jobs/4892802076/artifacts/target/site/index.html
# Pipelines link: https://gitlab.com/group-of-users/cucumberserenity/-/pipelines

Instructions on how to install, run and write new tests
# To create tests for new feature, user should add new .feature file to the src/test/resources/features folder. This Feature
# and each Scenario in it can be tagged with any @Tag to run via Cucumber (optional) and must contain Gherkin scripts. 
# For the created feature file, related StepDefinition.java must be created in src/test/java/cucumber_step_defs folder
# where each step must be implemented.
#
# Common steps may be stored in BaseTest.java and tagged with Serenity @Step tag. BaseTest object and/or similar common steps
# class must be tagged with Serenity @Steps tag to use inside any StepDefinition.class. You can run certain scenarios 
# by using this tags via cucumber command line or by configuring and running RunnerTest.class.
#
# Features and scenarios can be run via IDE inside the .feature files or specific pipeline parametrization can be added via
# .gitlab-ci.yml.
#
# User can also log any step or any action by using latest version of log4j2 dependency.
#
# To generate Serenity report locally, please use Maven -> plugins -> serenity -> serenity:aggregate lifecycle phase after test run. 

Refactoring summary
# For proper usage of the framework and to avoid unexpected conflicts, Gradle builder has been removed from framework
# and Maven builder remained as per requirements.
#
# Inside the pom.xml proper versions of plugins and dependencies were chosen, so they can interact between each other.
#
# Duplicated README file has been removed; serenity.properties updated with reporting settings; src/test/resources folder now
# contains only features folder with related .feature files.
#
# .gitignore file updated and now covers only necessary folders for CI run, including .gitignore file itself.
#
# TestRunner.java was renamed to RunnerTest to increase stability of Cucumber tool. @CucumberOptions updated with glue 
# folder parameter (to run our tests with BDD approach), cucumber reporting was enabled in addition.
#
# In post_product.feature file, steps were renamed and generalized. Positive and negative scenarios were defined.
# For that purpose Scenario and Scenario Outline approaches were used.
#
# In SearchStepDefinitions.java file, steps were reimplemented. SerenityRest methods and JUnit Assert methods were used here. 
# One similar action was reused via BaseTest.java as an example.

Serenity report
# You can find serenity report by the link above, or you can open the pipelines page, choose run -> artifacts -> target -> site
# -> index.html and check it after each run.