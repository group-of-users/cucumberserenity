@Search
Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario Outline: Positive scenarios
    When he calls endpoint for product "<product>"
    Then he sees the results displayed
    Examples:
      | product |
      | orange |
      | apple |
      | pasta |
      | cola |

  Scenario: Negative scenario
    When he calls endpoint for product "car"
    Then he does not see the results but error instead
    And message that item not found will be displayed
