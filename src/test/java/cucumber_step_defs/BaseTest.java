package cucumber_step_defs;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BaseTest {
    private final Logger log = LogManager.getLogger(this);

    public static String BASE_URI = "https://waarkoop-server.herokuapp.com/api/v1/search/demo/";

    @Step
    public void commonSeeResultsStep() {
        String responseString = SerenityRest.then().extract().response().asString();
        log.debug("API RESPONSE: " + responseString);
    }
}