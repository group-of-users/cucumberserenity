package cucumber_step_defs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SearchStepDefinitions {

    public Response response;

    @Steps
    public BaseTest baseTest;

    @When("he calls endpoint for product {string}")
    public void heCallsEndpointForProduct(String path) {
        response = SerenityRest.given().get(BaseTest.BASE_URI + path);
    }

    @Then("he sees the results displayed")
    public void heSeesTheResultsDisplayed() {
        restAssuredThat(response -> response.statusCode(200));
        baseTest.commonSeeResultsStep();
    }

    @Then("he does not see the results but error instead")
    public void heDoesNotSeeTheResultsButErrorInstead() {
        restAssuredThat(response -> response.statusCode(404));
        boolean errorBoolean = response.jsonPath().getBoolean("detail.error");
        assertTrue(errorBoolean);
    }

    @And("message that item not found will be displayed")
    public void messageThatItemNotFoundWillBeDisplayed() {
        String errorMessage = response.jsonPath().getString("detail.message");
        assertEquals("Not found", errorMessage);
    }
}
